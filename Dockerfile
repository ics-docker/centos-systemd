FROM centos:centos7

LABEL maintainer "te-hung.tseng@ess.eu"

ENV container docker

# Delete a number of unit files which might cause issues
# when running systemd inside docker
# See https://hub.docker.com/_/centos/
RUN (cd /lib/systemd/system/sysinit.target.wants/; for i in *; do [ $i == \
  systemd-tmpfiles-setup.service ] || rm -f $i; done); \
  rm -f /lib/systemd/system/multi-user.target.wants/*;\
  rm -f /etc/systemd/system/*.wants/*;\
  rm -f /lib/systemd/system/local-fs.target.wants/*; \
  rm -f /lib/systemd/system/sockets.target.wants/*udev*; \
  rm -f /lib/systemd/system/sockets.target.wants/*initctl*; \
  rm -f /lib/systemd/system/basic.target.wants/*;\
  rm -f /lib/systemd/system/anaconda.target.wants/*;

RUN yum-config-manager --disable base extras updates
COPY CentOS-ICS.repo /etc/yum.repos.d/CentOS-ICS.repo
RUN yum-config-manager --enable C7.9.2009*

# Install iproute to get Ansible facts properly populated
RUN yum install -y iproute

VOLUME [ "/sys/fs/cgroup" ]

CMD ["/usr/sbin/init"]
