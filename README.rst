centos-systemd
==============

This Dockerfile creates a Docker_ image with systemd enabled.

This image allows to test Ansible_ roles that use the *service* or *systemd* module
in a docker container (using Molecule_).
The goal is not to deploy an application in a docker container running systemd.

This is for testing purpose only.

Building
--------

This image is built automatically by GitLab.


.. _Docker: https://www.docker.com
.. _Ansible: http://docs.ansible.com/ansible/
.. _Molecule: https://molecule.readthedocs.io
